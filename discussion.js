//CRUD Operations

//Inser Documents (CREATE)
/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})
		Insert Many Document
			db.collectionName.insertMany([
				
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			}

			])

*/
//inserting one document
db.users.insertOne({
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21,
		"email": "janedoe!mail.com",
		"department": "none"
})


db.users.insertMany([
	{
	"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "none"
	}


	]);
//-----------------
//make new collection with the name courses
//insert the following fields and values
db.users.insertMany([
{
		"name": "Javascript 101",
			"price": "5000",
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{

			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{

			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false

		}
		])

//Find Documents (Read)
/*
	Syntax:
		db.collectionName.find() - this will retrieve all our documents
		db.collectionName.(find)({"criteria": "value"}) - this will retrieve all our documents that will match with our criteria

		db.collectionName.findOne({}) will return the first document in our collection

		db.collectionName.findOne({"criteria: "value"})- will return the first document in our collection that will match our criteria

*/
db.users.find():

db.users.find({
	"firstName": "Jane"
});

db.users.findOne()

db.users.findOne({
	"department": "none"
})

//Update Documents (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set:{
				"fieldToBeUpdate": "updatedValue"
			}
		})
*/
//--------------------
//update many
//Syntax:
		db.collectionName.updateMany({
			"criteria": "value"
		},
		{
			$set:{
				"fieldToBeUpdate": "updatedValue"
			}
		})

db.users.insertOne({
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "test@mail.com",
		"department": "none"
})

//Updating One Document

db.users.updateOne(
{
	"firstname": "Test"
},
{
	$set: {

		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "Operations",
		"status": "active"

	}
}
);

//Updating Multiple Documents
db.users.updateMany(
{
	"department": "none"
},
{
	$set: {
		"department": "HR"
	}
}
);

db.users.updateOne(
{
	"firstName": "Jane",
	"lastName":"Doe"
},
{
	$set: {
		"Department": "HR"
	}
}
);
//removing  FIELD
db.users.updateOne({
	"firstName": "Bill"
},
{
	$unset: {
		"status": "active"
	}
});

//-----------------
//Renaming field
db.users.updateMany(
{},
{
	$rename:{
		"department": "dept"
	}
}
);
/*

Mini Activity
	1. In our courses collection, update the HTML 101 COURSE
		-Make the isActive to false
	2. Add enrollees field to all the document in our collection
	-Enrollees




*/
db.users.insertMany([
{
		"name": "Javascript 101",
			"price": "5000",
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{

			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{

			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false

		}
		]);
//-
db.users.updateOne(
{
	"name": "HTML 101"
},
{
	$set: {
		"isActive": "false"
	}
}
);
//Deleting Documents
/*
	Syntax:
		-db.collectionName.deleteOne({"criteria: value"})
		-db.collectionName.deleteMany({"criteria: value"})

*/
db.users.insertOne({
	"firtsName": "Test"
});
//------------

//delete test
db.users.deleteOne({"firstName": "Test"});
db.users.deleteMany({"dept": "HR"});
db.courses.deleteMany({})
